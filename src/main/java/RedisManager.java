package main.java;

import main.java.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

/**
 * Created by Matt on 20/04/2015.
 */
public class RedisManager {

    final Jedis jedis = new Jedis("localhost");

    private static RedisManager instance = new RedisManager();

    public static RedisManager getInstance() {
        return instance;
    }

    public Jedis getJedis() {
        return jedis;
    }
}
