package main.java.rank;

import main.java.SMCore;
import org.bukkit.ChatColor;

import java.text.MessageFormat;


/**
 * Created by Matt on 21/04/2015.
 */
public enum Rank {

    DEV("DEV", ChatColor.DARK_PURPLE),
    OWNER("OWNER", ChatColor.GOLD),
    MOD("MOD", ChatColor.DARK_GREEN),
    HELPER("HELPER", ChatColor.YELLOW),
    YOUTUBER("YOUTUBER", ChatColor.LIGHT_PURPLE),
    ADMIN("ADMIN", ChatColor.RED),
    BUILDER("BUILDER", ChatColor.DARK_AQUA),
    DEFAULT("DEFAULT", ChatColor.GRAY),
    CUB("CUB", ChatColor.AQUA),
    KING("KING", ChatColor.GREEN);

    String name;
    ChatColor color;
    ChatColor bold;
    Rank(String name, ChatColor color){
        this.name = name;
        this.bold = ChatColor.BOLD;
        this.color = color;
    }


    public ChatColor getColor() {
        return color;
    }

    public String getName() {
        return name;
    }
}
