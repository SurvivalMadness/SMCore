package main.java.rank;

import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

/**
 * Created by Matt on 21/04/2015.
 */
public class RankManager {

    private static RankManager instance = new RankManager();

    public static RankManager getInstance() {
        return instance;
    }

    public void setRank(Player pl, Rank rank) {
        Statement s = null;

        Rank plRank = getRank(pl);

        try (Connection con = SMCore.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            if (plRank != null) {
                s.executeUpdate("UPDATE playerData SET rank='" + rank.getName() + "' WHERE uuid='" + pl.getUniqueId() + "'");
            } else {
                s.executeUpdate("INSERT INTO playerData (`uuid`, `rank`) VALUES ('" + pl.getUniqueId() + "', '" + rank.getName() + "');");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted info");
        PlayerManager.getInstance().getPlayer(pl).setRank(rank);
        PlayerManager.getInstance().getPlayer(pl).setRankName(getFullName(rank));
    }


    public void setRank(UUID uuid, Rank rank) {
        Statement s = null;
        Rank plRank = getRank(uuid);

        try (Connection con = SMCore.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            if (plRank != null) {
                s.executeUpdate("UPDATE playerData SET rank='" + rank.getName() + "' WHERE uuid='" + uuid + "'");
            } else {
                s.executeUpdate("INSERT INTO playerData (`uuid`, `rank`) VALUES ('" + uuid + "', '" + rank.getName() + "');");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted info");
    }


    public Rank getRank(Player pl) {
        Statement s = null;
        ResultSet res = null;

        try (Connection con = SMCore.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            res = s.executeQuery("SELECT `rank` as r FROM `playerData` WHERE uuid='" + pl.getUniqueId() + "'");
            if (res.next()) {
                if (res.getString("r") == null) {
                    return null;
                } else {
                    return getRankFromName(res.getString("r"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Rank getRank(UUID uuid) {
        Statement s = null;
        ResultSet res = null;

        try (Connection con = SMCore.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            res = s.executeQuery("SELECT `rank` as r FROM `playerData` WHERE uuid='" + uuid + "'");
            if (res.next()) {
                if (res.getString("r") == null) {
                    return null;
                } else {
                    return getRankFromName(res.getString("r"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Rank getRankFromName(String name) {
        for (Rank rank : Rank.values()) {
            if (rank.name.equalsIgnoreCase(name)) {
                return rank;
            }
        }
        return null;
    }

    public static boolean hasCubRank(Player pl) {
        SMPlayer splayer = PlayerManager.getInstance().getPlayer(pl);
        if (splayer.getRank() == Rank.CUB || splayer.getRank() == Rank.KING || splayer.getRank() == Rank.HELPER || splayer.getRank() == Rank.MOD || splayer.getRank() == Rank.DEV || splayer.getRank() == Rank.ADMIN || splayer.getRank() == Rank.OWNER) {
            return true;
        }
        return false;
    }

    public static boolean hasKingRank(Player pl) {
        SMPlayer splayer = PlayerManager.getInstance().getPlayer(pl);
        if (splayer.getRank() == Rank.KING || splayer.getRank() == Rank.HELPER || splayer.getRank() == Rank.MOD || splayer.getRank() == Rank.DEV || splayer.getRank() == Rank.ADMIN || splayer.getRank() == Rank.OWNER) {
            return true;
        }
        return false;
    }

    public static boolean hasStaffRank(Player pl) {
        SMPlayer splayer = PlayerManager.getInstance().getPlayer(pl);
        if (splayer.getRank() == Rank.HELPER || splayer.getRank() == Rank.MOD || splayer.getRank() == Rank.DEV || splayer.getRank() == Rank.ADMIN || splayer.getRank() == Rank.OWNER) {
            return true;
        }
        return false;
    }

    public static boolean hasAdminRank(Player pl) {
        SMPlayer splayer = PlayerManager.getInstance().getPlayer(pl);
        if (splayer.getRank() == Rank.OWNER || splayer.getRank() == Rank.DEV || splayer.getRank() == Rank.ADMIN) {
            return true;
        }
        return false;
    }

    public String getFullName(Rank rank) {
        return rank.color + rank.name;
    }
}
