package main.java.game;

/**
 * Created by Matt on 20/04/2015.
 */
public enum GameState {
    JOIN, VOTING, OFFLINE, INGAME;
}
