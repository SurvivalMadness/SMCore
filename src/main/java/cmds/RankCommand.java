package main.java.cmds;

import main.java.RedisManager;
import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.rank.Rank;
import main.java.rank.RankManager;
import main.java.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.PlayerNamePrompt;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Created by Matt on 29/05/2015.
 */
public class RankCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String s, final String[] args) {
        if (cmd.getName().equalsIgnoreCase("setrank")) {
            if (sender instanceof Player) {
                final Player pl = (Player) sender;
                if (!(RankManager.getInstance().hasAdminRank(pl)) || pl.getUniqueId().toString() != "4207d8ec-2792-4061-bb26-4463f850a2e6" || pl.getUniqueId().toString() != "4b591e4d-7d12-4d2d-b091-935a0a7b1290") {
                    pl.sendMessage(SMCore.noperm);
                    return true;
                } else {
                    if (args.length != 2) {
                        pl.sendMessage(ChatColor.RED + "Improper usage: /setrank <player> <rank>");
                    } else {
                        final Rank rank = RankManager.getInstance().getRankFromName(args[1]);
                        if (rank == null) {
                            pl.sendMessage(SMCore.staff + ChatColor.RED + "No rank with that name!");
                            return true;
                        } else {
                            Player target = Bukkit.getPlayer(args[0]);
                            if (target == null) {
                                new BukkitRunnable() {
                                    public void run() {
                                        try {
                                            UUID uuid = UUIDFetcher.getUUIDOf(args[0]);
                                            RankManager.getInstance().setRank(uuid, rank);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.runTaskAsynchronously(SMCore.getPlugin());
                                pl.sendMessage(SMCore.color(SMCore.staff + "&e" + args[0] + " &cis currently not online, but their rank has been updated."));
                                return true;
                            }
                            else {
                                PlayerManager.getInstance().getPlayer(target).setRank(rank);
                                PlayerManager.getInstance().getPlayer(target).setRankName(RankManager.getInstance().getFullName(rank));
                                RankManager.getInstance().setRank(target , rank);
                                pl.sendMessage(SMCore.color(SMCore.staff + "&a" + args[0] + "&e's rank has been set to " + rank.getColor() + rank.getName()));
                                return true;
                            }
                        }
                    }
                }
            }
            else {
                if (args.length != 2) {
                    sender.sendMessage(ChatColor.RED + "/setrank playername rank");
                } else {
                    final Rank rank = RankManager.getInstance().getRankFromName(args[1]);
                    if (rank == null) {
                        sender.sendMessage(ChatColor.RED + "No rank with that name!");
                        return true;
                    } else {
                        Player target = Bukkit.getPlayer(args[0]);
                        if (target == null) {
                            new BukkitRunnable() {
                                public void run() {
                                    try {
                                        UUID uuid = UUIDFetcher.getUUIDOf(args[0]);
                                        //TODO RankManager.getInstance().setRank(uuid, rank);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.runTaskAsynchronously(SMCore.getPlugin());
                            sender.sendMessage(ChatColor.GREEN + "Cached player: " + args[0] + "'s rank as they are not online!");
                            return true;
                        }
                        else {
                            PlayerManager.getInstance().getPlayer(target).setRank(rank);
                            PlayerManager.getInstance().getPlayer(target).setRankName(RankManager.getInstance().getFullName(rank));
                            RankManager.getInstance().setRank(target , rank);
                            sender.sendMessage(ChatColor.GREEN + "set player: " + args[0] + "'s rank to: " + rank.name() + "!");
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
