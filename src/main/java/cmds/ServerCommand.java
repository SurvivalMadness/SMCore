package main.java.cmds;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import main.java.SMCore;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matt on 02/06/2015.
 */
public class ServerCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(cmd.getName().equalsIgnoreCase("leave")){
            if(!(sender instanceof Player)){
                sender.sendMessage("Error: Console cannot change servers");
                return true;
            }

            Player player = (Player) sender;

            if(args.length > 0){
                return true;
            }

            if(Bukkit.getServer().getServerName().startsWith("hub")){
                player.sendMessage(ChatColor.RED + "You cannot leave the hub!");
                return true;
            }
            else {
                String hub = "hub";
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF(hub);
                player.sendPluginMessage(SMCore.getPlugin(), "BungeeCord", out.toByteArray());
            }
        }
        return false;
    }
}
