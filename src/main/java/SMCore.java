package main.java;

import main.java.cmds.RankCommand;
import main.java.cmds.ServerCommand;
import main.java.game.GameState;
import main.java.listeners.*;
import main.java.player.PlayerManager;
import main.java.server.ServerManager;
import main.java.utils.MySQL;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Matt on 05/04/2015.
 */
public class SMCore extends JavaPlugin {

    public static String color(String color) {
        return ChatColor.translateAlternateColorCodes('&', color);
    }

    public static String prefix = SMCore.color("&e&lSURVIVAL &b&LMADNESS &c- &r");
    public static String staff = SMCore.color("&c&lSTAFF &b> &r");
    public static String noperm = org.bukkit.ChatColor.RED + "I am sorry, but you have 99 problems, and not having this permission is now one!";

    public static String bold = ChatColor.BOLD.toString();

    private static SMCore plugin;

    public MySQL sql = new MySQL(plugin, "localhost", "3306", "playerData", "root", "t967vsTzA3");
    Connection c = null;
    Statement s = null;

    public Connection getC() {
        return c;
    }

    public void setC() {
        new BukkitRunnable() {
            public void run() {
                try {
                    c = sql.openConnection();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(this);
    }

    public void onEnable() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        plugin = this;
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        System.out.println(getServer().getServerName());
        pm.registerEvents(new PlayerChat(), this);
        pm.registerEvents(new FoodLevelChange(), this);
        pm.registerEvents(new PlayerCommandPreprocess(), this);
        getCommand("setrank").setExecutor(new RankCommand());
        getCommand("leave").setExecutor(new ServerCommand());

        for (Player pl : Bukkit.getOnlinePlayers()) {
            PlayerManager.getInstance().loadPlayer(pl);
        }
        ServerManager.getInstance().addServer(Bukkit.getServer().getServerName());
        ServerManager.getInstance().loadAllServers();
        ServerManager.getInstance().loadHubs();
    }

    public void onDisable() {
        plugin = null;
    }

    public static SMCore getPlugin() {
        return plugin;
    }

    public RedisManager getRedis() {
        return RedisManager.getInstance();
    }

    public MySQL getSql() {
        return sql;
    }
}
