package main.java.server;

import main.java.game.GameState;
import main.java.player.SMPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matt on 21/06/2015.
 */
public class PlayableServer {

    String name;
    List<SMPlayer> players;
    int onlineCount;
    GameState state;

    public PlayableServer(String name){
        this.name = name;
        this.players = new ArrayList<>();
        this.onlineCount = players.size();
        state = GameState.JOIN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SMPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(List<SMPlayer> players) {
        this.players = players;
    }

    public int getOnlineCount() {
        return onlineCount;
    }

    public void setOnlineCount(int onlineCount) {
        this.onlineCount = onlineCount;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }
}
