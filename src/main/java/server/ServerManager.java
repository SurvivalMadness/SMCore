package main.java.server;

import main.java.RedisManager;
import main.java.game.GameState;
import main.java.player.SMPlayer;

import java.util.*;

/**
 * Created by Matt on 21/06/2015.
 */
public class ServerManager {

    private static ServerManager instance = new ServerManager();

    public static ServerManager getInstance(){
        return instance;
    }

    List<HubServer> hubs = new ArrayList<>();
    List<PlayableServer> allServers = new ArrayList<>();

    public void loadHubs() {
        for(String s : RedisManager.getInstance().getJedis().keys("server:*")){
            String newServer = s.replace("server:", "");
            if(newServer.contains("hub")){
                //This is a hub server
                HubServer hubServer = new HubServer(newServer);
                hubs.add(hubServer);
            }
            return;
        }
    }

    public List<HubServer> getHubs(){
        return hubs;
    }


    public GameState getState(PlayableServer server){
        return GameState.valueOf(RedisManager.getInstance().getJedis().hget("server:" + server.getName(), "state"));
    }

    public void setState(PlayableServer server, GameState state){
        RedisManager.getInstance().getJedis().hset("server:" + server.getName(), "state", state.toString());
    }

    public void loadAllServers() {
        for (String s : RedisManager.getInstance().getJedis().keys("server:*")) {
            String newServer = s.replace("server:", "");
            PlayableServer server = new PlayableServer(newServer);
            allServers.add(server);
        }
    }

    public void addServer(String serverName) {
        PlayableServer server = new PlayableServer(serverName);
        Map<String, String> data = new HashMap<>();
        data.put("name", serverName);
        data.put("state", server.getState().toString());
        data.put("online", Integer.toString(server.getOnlineCount()));
        allServers.add(server);
        RedisManager.getInstance().getJedis().hmset("server:" + serverName, data);
        RedisManager.getInstance().getJedis().save();
        if (server instanceof HubServer) {
            hubs.add((HubServer) server);
        }
    }

    public void addToPlayerCount(PlayableServer server){
        Integer amount = getOnlinePlayers(server);
        RedisManager.getInstance().getJedis().hset("server:" + server.getName(), "online", String.valueOf(amount + 1));
    }

    public int getOnlinePlayers(PlayableServer server){
        return Integer.parseInt(RedisManager.getInstance().getJedis().hget("server:" + server.getName(), "online"));
    }

    public void removePlayers(PlayableServer server){
        Integer amount = getOnlinePlayers(server);
        RedisManager.getInstance().getJedis().hset("server:" + server.getName(), "online", String.valueOf(amount - 1));
    }

    public PlayableServer getServer(String serverName) {
        for (PlayableServer server : allServers) {
            if (server.getName().equalsIgnoreCase(serverName)) {
                return server;
            }
        }
        return null;
    }

    public List<PlayableServer> getAllServers(){
        return allServers;
    }
}
