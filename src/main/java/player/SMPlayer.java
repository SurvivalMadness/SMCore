package main.java.player;

import main.java.SMCore;
import main.java.rank.Rank;
import main.java.rank.RankManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import java.util.UUID;

/**
 * Created by Matt on 02/06/2015.
 */
public class SMPlayer {

    Player player;
    String lastName;
    String currentName;
    UUID uuid;
    Rank rank;
    String rankName;
    String currentServer;

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    public String getCurrentServer() {
        return currentServer;
    }

    public SMPlayer(Player player){
        if(!player.getName().equals(lastName)){
            lastName = player.getName();
            currentName = player.getName();
        }

        uuid = player.getUniqueId();
        rank = RankManager.getInstance().getRank(player);
        rankName = RankManager.getInstance().getFullName(rank);
        currentServer = Bukkit.getServer().getServerName();

    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCurrentName() {
        return currentName;
    }

    public void setCurrentName(String currentName) {
        this.currentName = currentName;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void sendMessage(String message) {
        player.sendMessage(SMCore.color(message));
    }

    public String getRankPrefix() {
        return rank.getColor() + ChatColor.BOLD.toString() + rank.getName();
    }

    public void setScoreboard(Scoreboard board){
        player.setScoreboard(board);
    }

}
