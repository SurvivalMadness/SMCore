package main.java.player;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matt on 02/06/2015.
 */
public class PlayerManager {

    private static PlayerManager instance = new PlayerManager();

    public static PlayerManager getInstance(){
        return instance;
    }

    List<SMPlayer> players = new ArrayList<>();

    public void loadPlayer(Player player){
        players.add(new SMPlayer(player));
    }

    public void unloadPlayer(Player player){
        SMPlayer pl = new SMPlayer(player);

        if(players.contains(pl)){
            players.remove(pl);
        }
    }

    public SMPlayer getPlayer(Player player){
        for(SMPlayer smplayer : players){
            if(smplayer.getUuid() == player.getUniqueId()){
                return smplayer;
            }
        }
        return null;
    }
}
