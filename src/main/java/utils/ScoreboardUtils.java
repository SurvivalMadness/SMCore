package main.java.utils;

import main.java.player.SMPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.*;

/**
 * Created by Matt on 20/06/2015.
 */
public class ScoreboardUtils {

    public static void setNormalScoreboard(SMPlayer player){
        ScoreboardManager sbm = Bukkit.getScoreboardManager();
        Scoreboard board = sbm.getNewScoreboard();
        Objective obj = board.registerNewObjective("dummy", "test");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(ChatColor.GOLD + "Survival Madness");
        Score server = obj.getScore(ChatColor.WHITE + "Server: " + ChatColor.GREEN + player.getCurrentServer());
        server.setScore(-1);
        player.setScoreboard(board);
    }
}
