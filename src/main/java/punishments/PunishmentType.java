package main.java.punishments;

import main.java.SMCore;

import java.util.concurrent.TimeUnit;

/**
 * Created by Matt on 14/06/2015.
 */
public enum PunishmentType {

    BAN_KILLAURA("Kill Aura", SMCore.color("&c&lBANNED!\n\n&fYou have been banned for the use of Kill Aura.\n\nBan time: &c3 months"), "3m"),
    BAN_FORCEFIELD("FORCEFIELD", SMCore.color("&c&lBANNED!\n\n&fYou have been banned for the use of Forcefield.\n\nBan time: &c3 months"), "3m"),
    BAN_XRAY("XRAY", SMCore.color("&c&lBANNED!\n\n&fYou have been banned for the use of X-RAY.\n\nBan time: &c1 month"), "1m"),
    BAN_DISRESPECT("DISRESPECT", SMCore.color("&c&lBANNED!\n\n&fYou have been banned for disrespecting players.\n\nBan time: &c2 weeks"), "2w"),
    BAN_STAFFDISRESEPCT("STAFF_DISRESPECT", SMCore.color("&c&lBANNED!\n\n&fYou have been banned for disrespecting staff.\n\nBan time: &c1 month"), "1m"),
    BAN_FLY("FLY", SMCore.color("&c&lBANNED!\n\n&fYou have been banned for the use of Flying Modifications.\n\nBan time: &c2 months"), "2m"),
    BAN_AIMBOT("AIMBOT", SMCore.color("&c&lBANNED!\n\n&fYou have been banned for the use of Aimbot.\n\nBan time: &c2 months"), "2m");


    String name;
    String reason;
    String time;
    PunishmentType(String name, String reason, String time){
        this.name = name;
        this.reason = reason;
        this.time = time;
    }

    public String getName(){
        return name;
    }

    public String getReason(){
        return reason;
    }

    public String getTime(){
        return time;
    }
}
