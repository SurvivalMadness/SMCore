package main.java.punishments;

import main.java.SMCore;
import main.java.player.SMPlayer;
import main.java.rank.Rank;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Matt on 14/06/2015.
 */
public class PunishmentManager {

    private static PunishmentManager instance = new PunishmentManager();

    public static PunishmentManager getInstance(){
        return instance;
    }

    public void tempBan(SMPlayer player, PunishmentType type) {
        Punishment punishment = new Punishment(getAvailibleID(), player, type, System.currentTimeMillis(), stringToSeconds(type.getTime()), false);
        add(punishment);
    }

    public void kick(SMPlayer player, PunishmentType type) {
        Punishment punishment = new Punishment(getAvailibleID(), player, type, System.currentTimeMillis(), 0, true);
        
    }

    public void add(Punishment punishment) {
        Statement s = null;

        try (Connection con = SMCore.getPlugin().getSql().openConnection()) {
            s = con.createStatement();
            s.executeUpdate("INSERT INTO bans (`uuid`, `reason`, `id`, `time`, `duration`, `undone`) VALUES ('" + punishment.getPlayer().getUuid() + "', '" + punishment.getType().getReason() +
                    "', '" + punishment.getId() + "', '" + punishment.getStart() + "', '" + punishment.getDuration() + "', '" + Boolean.valueOf(punishment.isUndone()) + "');");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted info");
    }

    public int getAvailibleID() {
        Statement s = null;
        ResultSet set = null;

        try (Connection con = SMCore.getPlugin().getSql().openConnection()) {
            s = con.createStatement();
            set = s.executeQuery("SELECT COUNT(*) as c FROM `playerData`");
            if (set.next()) {
                if (set.getInt("c") == 0) {
                    return 0;
                } else {
                    return (set.getInt("c") + 1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int stringToSeconds(String time) {
        int timeInSeconds = 0;
        if (time.contains("m")) {
            String newTime = time.replace("m", "");
            int timeInMinutes = Integer.parseInt(newTime);
            timeInSeconds = (timeInMinutes * 60);
        } else if (time.contains("h")) {
            String newTime = time.replace("h", "");
            int timeInMinutes = Integer.parseInt(newTime);
            //Convert to seconds
            timeInSeconds = (timeInMinutes * 3600);
        } else if (time.contains("d")) {
            String newTime = time.replace("d", "");
            int timeInMinutes = Integer.parseInt(newTime);
            //Convert to seconds
            timeInSeconds = timeInMinutes * 86400;
        } else if (time.contains("w")) {
            String newTime = time.replace("w", "");
            int timeInMinutes = Integer.parseInt(newTime);
            //Convert to seconds
            timeInSeconds = timeInMinutes * 604800;
        }
        return timeInSeconds;
    }
}
