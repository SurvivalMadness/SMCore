package main.java.punishments;

import main.java.player.SMPlayer;

/**
 * Created by Matt on 14/06/2015.
 */
public class Punishment {

    private PunishmentType type;
    private long id;
    private boolean undone;
    private long start;
    private SMPlayer player;
    private int duration;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Punishment(long id, SMPlayer player, PunishmentType type, long start, int duration, boolean undone){
        this.id = id;
        this.player = player;
        this.type = type;
        this.start = start;
        this.undone = undone;
        this.duration = duration;

    }

    public PunishmentType getType() {
        return type;
    }

    public void setType(PunishmentType type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isUndone() {
        return undone;
    }

    public void setUndone(boolean undone) {
        this.undone = undone;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public SMPlayer getPlayer() {
        return player;
    }

    public void setPlayer(SMPlayer player) {
        this.player = player;
    }
}
