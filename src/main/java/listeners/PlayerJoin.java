package main.java.listeners;

import main.java.RedisManager;
import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import main.java.rank.Rank;
import main.java.rank.RankManager;
import main.java.server.ServerManager;
import main.java.utils.ScoreboardUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 03/05/2015.
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        SMPlayer splayer = PlayerManager.getInstance().getPlayer(player);
        e.setJoinMessage(null);

        if (RankManager.getInstance().getRank(player) == null) {
            new BukkitRunnable() {
                public void run() {
                    RankManager.getInstance().setRank(player, Rank.DEFAULT);
                }
            }.runTaskLater(SMCore.getPlugin(), 5L);
        }

        PlayerManager.getInstance().loadPlayer(player);

        ScoreboardUtils.setNormalScoreboard(PlayerManager.getInstance().getPlayer(player));

        if (RankManager.getInstance().hasStaffRank(player)) {
            player.setOp(true);
        }

        if (Bukkit.getServer().getServerName().contains("hub")) {
            return;
        }
        ServerManager.getInstance().addToPlayerCount(ServerManager.getInstance().getServer(Bukkit.getServer().getServerName()));
        Bukkit.getServer().broadcastMessage("DEBUG1");
        if (splayer.getRank() == Rank.DEFAULT) {
            player.setPlayerListName(SMCore.color("&7" + player.getName()));
        } else {
            player.setPlayerListName(SMCore.color(splayer.getRank().getColor() + splayer.getRankPrefix() + " " + ChatColor.RESET + splayer.getRank().getColor() + splayer.getCurrentName()));
            // if name is more than 16+ characters, disable method.
        }
        Bukkit.getServer().broadcastMessage("DEBUG2");
    }
}
