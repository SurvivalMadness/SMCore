package main.java.listeners;

import main.java.RedisManager;
import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.server.ServerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Matt on 03/05/2015.
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        PlayerManager.getInstance().unloadPlayer(e.getPlayer());
        e.setQuitMessage(SMCore.color(SMCore.prefix + "&c " + e.getPlayer().getName() + " has disconnected."));
        if (Bukkit.getServer().getServerName().contains("hub")) {
            return;
        }
        ServerManager.getInstance().removePlayers(ServerManager.getInstance().getServer(Bukkit.getServer().getServerName()));
    }
}
