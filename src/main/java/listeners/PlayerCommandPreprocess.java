package main.java.listeners;

import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import main.java.rank.Rank;
import main.java.rank.RankManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Shiv on 6/14/2015.
 */
public class PlayerCommandPreprocess implements Listener {

    @EventHandler
    public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
        final Player p = e.getPlayer();
        SMPlayer pl = PlayerManager.getInstance().getPlayer(p);
        if (e.getMessage().startsWith("/plugins") || e.getMessage().startsWith("/plugins ") || e.getMessage().startsWith("/pl") || e.getMessage().startsWith("/pl ") || e.getMessage().startsWith("/bukkit:plugins") || e.getMessage().startsWith("/bukkit:plugins ")) {
            if (!RankManager.hasAdminRank(p)) {
                p.sendMessage(SMCore.noperm);
                e.setCancelled(true);
                return;
            }
            e.setCancelled(false);
        }

        if (e.getMessage().startsWith("/help")  || e.getMessage().startsWith("/help ") || e.getMessage().startsWith("/bukkit:help") || e.getMessage().startsWith("/bukkit:help ")) {
            pl.sendMessage(SMCore.color(SMCore.prefix + "&eThis command is not available to public players &e&lyet&r&e."));
            e.setCancelled(true);
            // TODO MAKE MENU
        }

        if(e.getMessage().startsWith("/ban") || e.getMessage().startsWith("/ban ")){
            String[] args = e.getMessage().split(" ");
            // TODO ENTIRE CMD
        }
    }
}
