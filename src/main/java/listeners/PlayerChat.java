package main.java.listeners;

import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import main.java.rank.Rank;
import main.java.rank.RankManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Matt on 29/05/2015.
 */
public class PlayerChat implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent e) {
        SMPlayer pl = PlayerManager.getInstance().getPlayer(e.getPlayer());

        String rawMessage = e.getMessage();
        String newMessage = rawMessage.replace("%", "%%");

        if (pl.getRank() == Rank.DEFAULT) {
            e.setFormat(SMCore.color(pl.getRank().getColor()  + " " + pl.getCurrentName() + "&6: &f" + newMessage));
        } else {
            e.setFormat(SMCore.color(pl.getRankPrefix() + " " + ChatColor.RESET + pl.getRank().getColor() + pl.getCurrentName() + "&6: &f" + newMessage));
            return;
        }
    }
}
